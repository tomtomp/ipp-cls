

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


from cpp_enums import TokenType


class CppToken:
    """
    This class represents one token.
    """

    def __init__(self, token_type, content=""):
        """
        Initialize token object.
        :param token_type: Type of the token, TokenType.
        :param content: Content of the token, string.
        :return:
        """

        if type(token_type) is TokenType:
            self._type = token_type
        else:
            raise Exception("Error, wrong parameter type!")
        self._content = content

    def get_type(self):
        """
        Get the type of the token.
        :return: TokenType
        """
        return self._type

    def get_content(self):
        """
        Get the content of the token.
        :return: string
        """
        return self._content

    def dump_info(self):
        """
        Dumps information about this token to the stdout.
        :return: None
        """
        print("Token type : ", self._type, "  \t| Content : ", self._content)
