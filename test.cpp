#include <iostream>

class A
{
public:
    int f1() {std::cout << "f1 A" << std::endl;};
private:
    int f2() {std::cout << "f2 A" << std::endl;};
protected:
};

class B : public A
{
public:
    int f1(int) {std::cout << "f1 B" << std::endl;}
    using A::f1;
private:
protected:
};

int main()
{
    B myB;
    myB.f1();
    return 0;
}
