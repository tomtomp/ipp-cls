

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""

import sys
from collections import defaultdict

from error_codes import print_info
from error_codes import print_err
from error_codes import ErrorCodes
from cpp_enums import AccessSpec
from cpp_enums import ClassT
from cpp_attribute import CppAttribute
from cpp_method import CppMethod
from cpp_inheritance import CppInheritance
from cpp_using import CppUsing


class CppClass:
    """
    This class represents a single C++ class and will contain all the
     information about it
    """

    def __init__(self, class_name):
        """
        Initialize C++ class object with its name.
        :param class_name: Name of the C++ class.
        :return:
        """
        self.__name = class_name
        self.__kind = ClassT.CONCRETE
        # Dictionary to contain the classes inherited from
        self.__inherits = {
            AccessSpec.PUBLIC: [],
            AccessSpec.PRIVATE: [],
            AccessSpec.PROTECTED: [],
        }
        # Dictionary to contain the class attributes
        self.__attributes = {
            AccessSpec.PUBLIC: set(),
            AccessSpec.PRIVATE: set(),
            AccessSpec.PROTECTED: set(),
        }
        # Dictionary to contain the class methods
        self.__methods = {
            AccessSpec.PUBLIC: set(),
            AccessSpec.PRIVATE: set(),
            AccessSpec.PROTECTED: set(),
        }

        # Dictionary to contain the class using directives
        self.__using = {
            AccessSpec.PUBLIC: [],
            AccessSpec.PRIVATE: [],
            AccessSpec.PROTECTED: [],
        }

        # Dictionary to contain the class constructors
        self.__constructors = {
            AccessSpec.PUBLIC: set(),
            AccessSpec.PRIVATE: set(),
            AccessSpec.PROTECTED: set(),
        }
        self.__destructor = None
        self.__destructor_access_spec = None

        # This dictionary will contain conflicts grouped by name. Each group is contained in a list.
        self.__conflicts = defaultdict(list)

    def process_usings(self, classes):
        """
        Process the using directives for this class.
        :param classes: The class dictionary.
        """

        for access_spec, using_dirs in self.__using.items():
            for using_dir in using_dirs:
                from_class = classes[using_dir.get_class_from()]
                member_name = using_dir.get_name()
                new_members = from_class.get_members_by_name(member_name)
                for new_member in new_members:
                    if type(new_member) is CppMethod:
                        self.add_method(new_member, access_spec)
                    elif type(new_member) is CppAttribute:
                        self.add_attribute(new_member, access_spec)
                    else:
                        print_err("Error, found unknown entity in new_members list.")
                        print_err(type(new_member))
                        sys.exit(ErrorCodes.ERR_INHERITANCE)

    def get_members_by_name(self, name):
        """
        Get all members (public and protected) with given name.
        :param name: Name to search for
        :return: List of members with given name.
        """
        results = []

        if self.get_name() == name:
            # Get constructors and destructor.
            for access_spec in (AccessSpec.PUBLIC, AccessSpec.PROTECTED):
                for constructor in self.__constructors[access_spec]:
                    results.append(constructor)
            if self.__destructor:
                results.append(self.__destructor)
            return results

        for access_spec in (AccessSpec.PUBLIC, AccessSpec.PROTECTED):
            for attribute in self.__attributes[access_spec]:
                if attribute.get_name() == name:
                    results.append(attribute)

            for method in self.__methods[access_spec]:
                if method.get_name() == name:
                    results.append(method)

        return results

    def inherit_from(self, other_class, access_spec, do_conflicts=False):
        """
        Inherit members from the given class, under given access specifier.
        :param other_class: Class from which to inherit, CppClass.
        :param access_spec: Access specifier, AccessSpec.
        :param do_conflicts: Should the conflicts be stored in conflicts dictionary, or should we just end the program.
        """

        print_info(self.__name, access_spec.value, "inheritance from", other_class.get_name())

        # Dictionary used for converting input access specifiers.
        access_conv = {
            AccessSpec.PUBLIC: AccessSpec.PUBLIC,
            AccessSpec.PRIVATE: AccessSpec.PRIVATE,
            AccessSpec.PROTECTED: AccessSpec.PROTECTED
        }

        if access_spec == AccessSpec.PROTECTED:
            access_conv[AccessSpec.PUBLIC] = AccessSpec.PROTECTED
        elif access_spec == AccessSpec.PRIVATE:
            access_conv[AccessSpec.PUBLIC] = AccessSpec.PRIVATE
            access_conv[AccessSpec.PROTECTED] = AccessSpec.PRIVATE

        # Inherit attributes.
        attr_dict = other_class.get_attribute_dict()

        for spec, attributes in attr_dict.items():
            # Get the derived access specifier
            result_spec = access_conv[spec]

            #if spec == AccessSpec.PRIVATE:
                #continue

            # For each attribute.
            for attribute in attributes:
                conflict = self.has_member_named(attribute.get_name(), True)
                if conflict:
                    # There is already a member of the same name inherited from other class.
                    # It may have gotten here through using directive.
                    using_dir = self.get_using_by_names(attribute.get_name())
                    if using_dir:
                        continue
                    else:
                        if do_conflicts:
                            self.__conflicts[attribute.get_name()].append(attribute)
                        else:
                            print_err("Found an attribute inheritance conflict!")
                            sys.exit(ErrorCodes.ERR_INHERITANCE)
                elif self.has_member_named(attribute.get_name(), False):
                    # The inherited member was overwritten.
                    pass
                else:
                    self.add_attribute(attribute, result_spec)

        # Inherit methods.
        meth_dict = other_class.get_method_dict()

        for spec, methods in meth_dict.items():
            # Get the derived access specifier
            result_spec = access_conv[spec]

            # For each attribute.
            for method in methods:
                if spec == AccessSpec.PRIVATE and not method.is_pure_virtual():
                    continue

                conflict = self.has_member_named(method.get_name(), True)
                if conflict:
                    # There is already a member of the same name inherited from other class.
                    # It may have gotten here through using directive.
                    using_dir = self.get_using_by_names(method.get_name())
                    if using_dir:
                        continue
                    else:
                        if do_conflicts:
                            self.__conflicts[method.get_name()].append(method)
                        else:
                            print_err("Found a method inheritance conflict!")
                            sys.exit(ErrorCodes.ERR_INHERITANCE)
                elif self.has_member_named(method.get_name(), False):
                    pass
                else:
                    self.add_method(method, result_spec)

    def remove_conflicted(self):
        """
        Remove the conflicted members.
        """

        conflicted_names = self.__conflicts.keys()

        for access_spec in (AccessSpec.PUBLIC, AccessSpec.PRIVATE, AccessSpec.PROTECTED):
            uni_set = set(att for att in self.__attributes[access_spec] if att.get_name() in conflicted_names)
            for att in uni_set:
                self.__conflicts[att.get_name()].append(att)
            #self.__attributes[access_spec].symmetric_difference_update(att for att in self.__attributes[access_spec]
                                                                             #if att.get_name() in conflicted_names)
            self.__attributes[access_spec].symmetric_difference_update(uni_set)
            uni_set = set(met for met in self.__methods[access_spec] if met.get_name() in conflicted_names)
            for att in uni_set:
                self.__conflicts[att.get_name()].append(att)
            #self.__methods[access_spec].symmetric_difference_update(met for met in self.__methods[access_spec]
                                                                          #if met.get_name() in conflicted_names)
            self.__methods[access_spec].symmetric_difference_update(uni_set)

    def get_xml_root(self, doc):
        """
        Get the root node of XML describing this class.
        :param doc: MiniDom document.
        :return: MiniDom node.
        """

        # Create the root
        node = doc.createElement("class")

        # Set base attributes
        node.setAttribute("name", self.__name)
        node.setAttribute("kind", self.__kind.value)

        # Process inheritance
        if self.is_inherits_not_empty():
            inherit_node = doc.createElement("inheritance")
            for access_spec, inheritances in self.__inherits.items():
                for inheritance in inheritances:
                    new_inherit = doc.createElement("from")
                    new_inherit.setAttribute("name", inheritance.get_name())
                    new_inherit.setAttribute("privacy", access_spec.value)
                    inherit_node.appendChild(new_inherit)
            node.appendChild(inherit_node)

        # Process members.
        for access_spec in (AccessSpec.PUBLIC, AccessSpec.PRIVATE, AccessSpec.PROTECTED):
            access_spec_node = doc.createElement(access_spec.value)

            # Process attributes.
            attributes_node = doc.createElement("attributes")
            for attribute in self.__attributes[access_spec]:
                if attribute.get_owner() != self.__name and \
                   attribute.get_access_spec() == AccessSpec.PRIVATE:
                    continue
                attributes_node.appendChild(attribute.get_xml_root(doc, self.__name))

            if attributes_node.hasChildNodes():
                access_spec_node.appendChild(attributes_node)

            # Process methods.
            methods_node = doc.createElement("methods")
            found_meth = True
            for method in self.__methods[access_spec]:
                methods_node.appendChild(method.get_xml_root(doc, self.__name))

            # Process constructors.
            for constructor in self.__constructors[access_spec]:
                methods_node.appendChild(constructor.get_xml_root(doc, self.__name))

            # Process destructor.
            if access_spec == self.__destructor_access_spec:
                methods_node.appendChild(self.__destructor.get_xml_root(doc, self.__name))

            if methods_node.hasChildNodes():
                access_spec_node.appendChild(methods_node)

            if methods_node.hasChildNodes() or attributes_node.hasChildNodes():
                node.appendChild(access_spec_node)

        # Process conflicts
        if self.__conflicts:
            conflicts_node = doc.createElement("conflicts")

            for confl_name, confl_list in self.__conflicts.items():
                conflict_node = doc.createElement("member")
                conflict_node.setAttribute("name", confl_name)

                # Used for aggregating multiple conflicts from one class.
                class_nodes = dict()

                for conflict in confl_list:
                    conflict_from_class = conflict.get_owner()
                    orig_access_spec = conflict.get_access_spec().value
                    try:
                        class_node = class_nodes[conflict_from_class]
                    except KeyError:
                        class_node = doc.createElement("class")
                        class_node.setAttribute("name", conflict_from_class)
                        class_nodes[conflict_from_class] = class_node
                        conflict_node.appendChild(class_node)

                    access_spec_node = class_node.getElementsByTagName(orig_access_spec)
                    if len(access_spec_node) < 1:
                        access_spec_node = doc.createElement(orig_access_spec)
                        class_node.appendChild(access_spec_node)
                    else:
                        access_spec_node = access_spec_node[0]

                    access_spec_node.appendChild(conflict.get_xml_root(doc, ""))

                conflicts_node.appendChild(conflict_node)

            node.appendChild(conflicts_node)


        return node

    def is_inherits_not_empty(self):
        """
        Is the inherits completely empty?
        :return: bool
        """

        return self.__inherits[AccessSpec.PUBLIC] or self.__inherits[AccessSpec.PRIVATE] or \
            self.__inherits[AccessSpec.PROTECTED]

    def get_using_by_names(self, name, class_name=""):
        """
        Get the using directive CppUsing object for given names.
        :param name: Name of the member.
        :param class_name: Name of the class from the using directive. Will search for any class when empty.
        :return: CppUsing or None.
        """
        for access_spec, usings in self.__using.items():
            for using in usings:
                if (using.get_class_from() == class_name or not class_name) and using.get_name() == name:
                    return using

        return None

    def __eq__(self, other):
        """
        Overloading equality operator.
        :param other: The other one.
        :return: bool
        """

        return self.get_name() == other.get_name()

    def __ne__(self, other):
        """
        Overloading inequality operator.
        :param other: The other one.
        :return: bool
        """

        return not (self == other)

    def __hash__(self):
        """
        Overloading hash operator.
        """

        return hash(self.get_name())

    def set_kind(self, kind=ClassT.ABSTRACT):
        """
        Sets the kind to the ClassT enum given.
        :param kind: ClassT enum.
        :return: None
        """
        if type(kind) is ClassT:
            self.__kind = kind
        else:
            raise Exception("Wrong parameter type")

    def has_member_named(self, name, not_from_this_class=False):
        """
        Does this class have any member named the same as given name?
        :param name: Name to search for.
        :param not_from_this_class: Do not count the members from this class.
        :return: Returns the object (CppAttribute, or CppMethod), or None, if none were found.
        """

        for access_spec, attributes in self.__attributes.items():
            for attribute in attributes:
                if attribute.get_name() == name:
                    if not_from_this_class:
                        if attribute.get_owner() != self.__name:
                            return attribute
                    else:
                        return attribute

        for access_spec, methods in self.__methods.items():
            for method in methods:
                if method.get_name() == name:
                    if not_from_this_class:
                        if method.get_owner() != self.__name:
                            return method
                    else:
                        return method

        return None

    def add_attribute(self, attribute, access_spec):
        """
        Add given attribute to the class, under given access specifier.
        :param attribute: Attribute, type CppAttribute.
        :param access_spec: Access specifier, type enum ClassT.
        :return: None
        """

        """
        if self.has_attribute(attribute):
            print_info("Found a conflict: ")
            attribute.dump_info()
        else:
        """

        if type(attribute) is CppAttribute and type(access_spec) is AccessSpec:
            if attribute in self.__attributes[access_spec]:
                self.__attributes[access_spec].remove(attribute)
            self.__attributes[access_spec].add(attribute)
        else:
            raise Exception("Wrong parameter type")

    def has_attribute(self, attribute, access_spec=None):
        """
        Does this CppClass have given attribute?
        :param attribute: Attribute, type CppAttribute.
        :param access_spec: Access specifier, type enum ClassT.
        :return: bool
        """

        if access_spec:
            return attribute in self.__attributes[access_spec]
        else:
            return attribute in self.__attributes[AccessSpec.PUBLIC] or \
                   attribute in self.__attributes[AccessSpec.PRIVATE] or \
                   attribute in self.__attributes[AccessSpec.PROTECTED]

    def get_attribute_dict(self):
        """
        Get the attribute dictionary of this class.
        :return: Dictionary containing AccessSpec: list of CppAttribute.
        """

        return self.__attributes

    def add_method(self, method, access_spec):
        """
        Add given method to the class, under given access specifier.
        :param method: Method, type CppMethod.
        :param access_spec: Access specifier, type enum ClassT.
        :return: None
        """

        if self.has_method(method):
            if method.is_virtual() and not method.is_defined():
                pass
            """
            else:
                print_info("Found a conflict: ")
                method.dump_info()
            """
        else:
            if method.is_virtual() and not method.is_defined():
                self.__kind = ClassT.ABSTRACT

            if method in self.__methods[access_spec]:
                self.__methods[access_spec].remove(method)

            if type(method) is CppMethod and type(access_spec) is AccessSpec:
                self.__methods[access_spec].add(method)
            else:
                raise Exception("Wrong parameter type")

    def has_method(self, method, access_spec=None):
        """
        Does this CppClass have given method?
        :param method: Method, type CppMethod.
        :param access_spec: Access specifier, type enum ClassT.
        :return: bool
        """

        if access_spec:
            return method in self.__methods[access_spec]
        else:
            return method in self.__methods[AccessSpec.PUBLIC] or \
                   method in self.__methods[AccessSpec.PRIVATE] or \
                   method in self.__methods[AccessSpec.PROTECTED]

    def get_method_dict(self):
        """
        Get the method dictionary of this class.
        :return: Dictionary containing AccessSpec: list of CppMethod.
        """

        return self.__methods

    def add_using(self, using, access_spec):
        """
        Add given using directive to the class, under given access specifier.
        :param using: Using, type CppUsing.
        :param access_spec: Access specifier, type enum ClassT.
        :return: None
        """
        if type(using) is CppUsing and type(access_spec) is AccessSpec:
            self.__using[access_spec].append(using)
        else:
            raise Exception("Wrong parameter type")

    def add_constructor(self, constructor, access_spec):
        """
        Add given constructor to the class, under given access specifier.
        :param constructor: Constructor, type CppMethod.
        :param access_spec: Access specifier, type enum ClassT.
        :return: None
        """
        if type(constructor) is CppMethod and type(access_spec) is AccessSpec:
            self.__constructors[access_spec].add(constructor)
        else:
            raise Exception("Wrong parameter type")

    def has_constructor(self, constructor, access_spec):
        """
        Does this CppClass have given constructor?
        :param constructor: Method, type CppMethod.
        :param access_spec: Access specifier, type enum ClassT.
        :return: bool
        """

        return constructor in self.__constructors[access_spec]

    def add_inheritance(self, inherit_class, access_spec, is_virtual):
        """
        Add given inherit class to the class, under given access specifier.
        :param inherit_class: Name of inherit class, type string.
        :param access_spec: Access specifier, type enum ClassT.
        :param is_virtual: Is the inheritance virtual?
        :return: None
        """

        self.__inherits[access_spec].append(CppInheritance(inherit_class, access_spec,
                                                           is_virtual))

    def get_inheritance_dict(self):
        """
        Get the inheritance dictionary, containts 3 keys (AccessSpec.PUBLIC, AccessSpec.PRIVATE and
            AccessSpec.PROTECTED). Values are lists of CppInheritance objects.
        :return: Dictionary.
        """

        return self.__inherits

    def set_destructor(self, destructor, access_spec):
        """
        Set the destructor method
        :param destructor: Method to become the destructor of this class, CppMethod.
        :param access_spec: Access specifier of the destructor, AccessSpec.
        :return: None
        """

        if type(destructor) is CppMethod:
            self.__destructor = destructor
        else:
            raise Exception("Wrong parameter type")

        if type(access_spec) is AccessSpec:
            self.__destructor_access_spec = access_spec
        else:
            raise Exception("Wrong parameter type")

    def get_destructor(self):
        """
        Get the destructor of the class.
        :return: CppMethod, or None, if there is no destructor.
        """
        return self.__destructor

    def get_name(self):
        """
        Get the name of the class.
        :return: string
        """
        return self.__name

    def get_kind(self):
        """
        Get the kind of the class. Can be concrete, or abstract.
        :return: ClassT
        """
        return self.__kind

    def dump_info(self):
        """
        Dumps information about this class to the stdout.
        :return: None
        """
        print("========================= CppClass : ", self.__name, " ============================================")
        print("|{}|Class type : ", self.__kind.value)
        print("|{}|Class inheritance : ")
        for access_spec, inherit_classes in self.__inherits.items():
            for inherit_class in inherit_classes:
                inherit_class.dump_info()
        print("|{}|Class attributes : ")
        for access_spec, attributes in self.__attributes.items():
            for attribute in attributes:
                attribute.dump_info()
        print("|{}|Class methods : ")
        for access_spec, methods in self.__methods.items():
            for method in methods:
                method.dump_info()
        print("|{}|Class using directives : ")
        for access_spec, usings in self.__using.items():
            for using in usings:
                using.dump_info()
        print("|{}|Class constructors : ")
        for access_spec, constructors in self.__constructors.items():
            for constructor in constructors:
                constructor.dump_info()
        if self.__destructor:
            print("|{}|Destructor : ")
            self.__destructor.dump_info()
        print("========================= End CppClass : ", self.__name, " ========================================")

