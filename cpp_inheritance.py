

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


class CppInheritance:
    """
    This class represents a single inheritance item.
    """

    def __init__(self, class_name, access_specifier, is_virtual):
        """
        Initialize inheritance object.
        :param class_name: Name of the inherited class, string.
        :param access_specifier: Access specifier of the inherited class.
        :param is_virtual: Is the class inherited virtually?
        :return:
        """

        self.__class_name = class_name
        self.__access_specifier = access_specifier
        self.__is_virtual = is_virtual

    def get_name(self):
        """
        Get the name of the class inherited from.
        :return: string
        """
        return self.__class_name

    def get_access_specifier(self):
        """
        Get the access specifier, under which is the class inherited from.
        :return: AccessSpec
        """
        return self.__access_specifier

    def is_virtual(self):
        """
        Is the inheritance virtual?
        :return: bool
        """
        return self.__is_virtual

    def dump_info(self):
        """
        Dumps information about this inheritance item.
        :return:
        """

        print(": ", end="")
        print(self.__access_specifier.name, " ", end="")
        if self.__is_virtual:
            print("virtual ", end="")
        print(self.__class_name)
