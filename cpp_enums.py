

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


from enum import Enum


class TokenType(Enum):
    """
    This class is enumeration of token types.
    """

    IDENT = 0
    KEYWRD = 1
    CURL_L = 2
    CURL_R = 3
    ROUND_L = 4
    ROUND_R = 5
    BRACK_L = 6
    BRACK_R = 7
    COLON = 8
    SEMICOLON = 9
    COMMA = 10
    TILDE = 11
    ASTER = 12
    AMPER = 13
    EQU = 14

    KW_CLASS = 15
    KW_VIRTUAL = 16
    KW_STATIC = 17
    KW_CONST = 18
    KW_PUBLIC = 19
    KW_PRIVATE = 20
    KW_PROTECTED = 21
    KW_USING = 22

    NUM = 23

    EOF = 24


class ClassT(Enum):
    """
    Represents the type of class.
    """
    ABSTRACT = "abstract"
    CONCRETE = "concrete"


class AccessSpec(Enum):
    """
    Represents the access specifier of method/attribute, or inheritance.
    """
    NONE = ""
    PUBLIC = "public"
    PRIVATE = "private"
    PROTECTED = "protected"
