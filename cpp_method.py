

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import traceback
import sys

from cpp_enums import AccessSpec
from cpp_attribute import CppAttribute


class CppMethod:
    """
    Represents a class method.
    """

    def __init__(self, ret_type, name, access_specifier, owner):
        """
        Initialize C++ method with given information.
        :param ret_type: Return type of the method, string.
        :param name: Name of the method, string.
        :param access_specifier: Access specifier of the method, AcessSpec.
        :param owner: The name of the owner class, string.
        :return:
        """

        self.__ret_type = ret_type
        self.__name = name
        self.__defined = False
        self.__static = False
        self.__virtual = False
        self.__owner = owner

        if type(access_specifier) is AccessSpec:
            self.__access_spec = access_specifier
        else:
            raise Exception("Wrong parameter type")

        # List, will contain objects of CppAttribute type and signifies the method arguments.
        self.__arguments = []

    def get_xml_root(self, doc, class_name):
        """
        Get the root node of XML describing this method.
        :param doc: MiniDom document.
        :param class_name: Name of the generating class, string.
        :return: MiniDom node.
        """

        method_node = doc.createElement("method")
        method_node.setAttribute("name", self.get_name())
        method_node.setAttribute("type", self.get_ret_type())
        if self.is_static():
            method_node.setAttribute("scope", "static")
        else:
            method_node.setAttribute("scope", "instance")
        if self.get_owner() != class_name and class_name != "":
            from_node = doc.createElement("from")
            from_node.setAttribute("name", self.get_owner())
            method_node.appendChild(from_node)
        if self.is_virtual():
            virt_node = doc.createElement("virtual")
            if self.is_defined():
                virt_node.setAttribute("pure", "no")
            else:
                virt_node.setAttribute("pure", "yes")
            method_node.appendChild(virt_node)
        arguments_node = doc.createElement("arguments")
        method_arguments = self.get_arguments()
        if len(method_arguments) == 1 and method_arguments[0].get_type() == "void":
            pass
        elif method_arguments:
            for argument in method_arguments:
                argument_node = doc.createElement("argument")
                argument_node.setAttribute("name", argument.get_name())
                argument_node.setAttribute("type", argument.get_type())
                arguments_node.appendChild(argument_node)
        else:
            arguments_node.appendChild(doc.createTextNode(""))
        method_node.appendChild(arguments_node)

        return method_node

    def __eq__(self, other):
        """
        Overloading equality operator.
        :param other: The other one.
        :return: bool
        """

        arg_types1 = ""

        for arg in self.__arguments:
            arg_types1 += arg.get_type()

        arg_types2 = ""

        for arg in other.get_arguments():
            arg_types2 += arg.get_type()

        return (self.get_name() + arg_types1) == (other.get_name() + arg_types2)

    def __ne__(self, other):
        """
        Overloading inequality operator.
        :param other: The other one.
        :return: bool
        """

        return not (self == other)

    def __hash__(self):
        """
        Overloading hash operator.
        """

        arg_types = ""

        for arg in self.__arguments:
            arg_types += arg.get_type()

        return hash(self.get_name() + arg_types)

    def get_owner(self):
        """
        Get name of the owner class.
        :return: string.
        """

        return self.__owner

    def add_argument(self, argument):
        """
        Add the given argument as argument of this method
        :param argument: Argument to add, CppAttribute.
        :return:
        """
        if type(argument) is CppAttribute:
            self.__arguments.append(argument)
        else:
            raise Exception("Wrong parameter type")

    def get_arguments(self):
        """
        Get the argument list.
        :return: List of CppAttribute.
        """

        return self.__arguments

    def set_defined(self):
        """
        Set the method as defined.
        :return: None
        """
        self.__defined = True

    def set_static(self):
        """
        Set the method as static.
        :return: None
        """
        self.__static = True

    def set_virtual(self):
        """
        Set the method as virtual.
        :return: None
        """
        self.__virtual = True

    def is_pure_virtual(self):
        """
        Is this method pure virtual?
        :return: bool
        """
        return self.__virtual and not self.__defined

    def is_defined(self):
        """
        Is the method defined?
        :return: bool
        """
        return self.__defined

    def is_static(self):
        """
        Is the method static?
        :return: bool
        """
        return self.__static

    def is_virtual(self):
        """
        Is the method virtual?
        :return: bool
        """
        return self.__virtual

    def get_ret_type(self):
        """
        Get the return type.
        :return: string
        """
        return self.__ret_type

    def get_name(self):
        """
        Get the name of the method.
        :return: string
        """
        return self.__name

    def get_access_spec(self):
        """
        Get the access specifier of the method
        :return: AccessSpec
        """
        return self.__access_spec

    def dump_info(self):
        """
        Dumps information about this method to the stdout.
        :return: None
        """

        """
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback)
        """

        print(self.__access_spec.value, " ", end="")
        if self.__virtual:
            print("virtual ", end="")
        if self.__static:
            print("static ", end="")
        print(self.__ret_type, " ", self.__name, " ( ")
        for arg in self.__arguments:
            print("-> ", end="")
            arg.dump_info()
        print(" ) ", end="")
        if self.__defined:
            print("{ defined } ", end="")
        else:
            print("UNDEFINED", end="")
        print(" owner :", self.__owner)

