#!/bin/python3

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""

from cpp_parser import CppParser


if __name__ == "__main__":
    app = CppParser()
    app.run()

    """
    except Exception as e:
        print_err("Message : ", e)
        exit(100)

    root = ET.Element('a')
    ET.SubElement(root, 'b')
    ET.SubElement(root, 'c')
    ET.SubElement(root, 'd')

    print(make_xml_pretty(root, 4))

    with open("cls.xml", "w", encoding="utf-8") as f:
        f.write(make_xml_pretty(root, 4))
    """

