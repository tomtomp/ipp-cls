

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


from cpp_enums import AccessSpec


class CppAttribute:
    """
    Represents a class attribute.
    """

    def __init__(self, att_type, name, access_spec, owner):
        """
        Initialize attribute object, using the information passed in.
        :param att_type: Type of attribute, string.
        :param name: Name of attribute, string.
        :param access_spec: Access specifier of attribute, AccessSpec.
        :param owner: Name of the owner class, string.
        :return:
        """

        self.__type = att_type
        self.__name = name
        self.__static = False
        self.__owner = owner

        if type(access_spec) is AccessSpec:
            self.__access_spec = access_spec
        else:
            raise Exception("Wrong parameter type")

    def get_xml_root(self, doc, class_name):
        """
        Get the root node of XML describing this attribute.
        :param doc: MiniDom document.
        :param class_name: Name of the generating class, string.
        :return: MiniDom node.
        """

        attribute_node = doc.createElement("attribute")
        attribute_node.setAttribute("name", self.get_name())
        attribute_node.setAttribute("type", self.get_type())
        if self.is_static():
            attribute_node.setAttribute("scope", "static")
        else:
            attribute_node.setAttribute("scope", "instance")
        if self.get_owner() != class_name and class_name != "":
            from_node = doc.createElement("from")
            from_node.setAttribute("name", self.get_owner())
            attribute_node.appendChild(from_node)

        return attribute_node

    def __eq__(self, other):
        """
        Overloading equality operator.
        :param other: The other one.
        :return: bool
        """

        return self.get_name() == other.get_name()

    def __ne__(self, other):
        """
        Overloading inequality operator.
        :param other: The other one.
        :return: bool
        """

        return not (self == other)

    def __hash__(self):
        """
        Overloading hash operator.
        """

        return hash(self.get_name())

    def get_owner(self):
        """
        Get the owner class name.
        :return: string
        """

        return self.__owner

    def set_static(self):
        """
        Set the attribute to static attribute.
        :return: None
        """
        self.__static = True

    def is_static(self):
        """
        Is the attribute/parameter static?
        :return: bool
        """
        return self.__static

    def get_type(self):
        """
        Get the type of the attribute/parameter.
        :return: string
        """
        return self.__type.strip()

    def get_name(self):
        """
        Get the name of the attribute/parameter.
        :return: string
        """
        return self.__name

    def get_access_spec(self):
        """
        Get the access specifier of the attribute.
        :return: AccessSpec
        """
        return self.__access_spec

    def dump_info(self):
        """
        Dumps information about this attribute to the stdout.
        :return: None
        """

        print(self.__access_spec.value, " ", end="")
        if self.__static:
            print("static ", end="")
        print(self.__type, " ", self.__name, " ", end="")
        print("owner :", self.__owner)

