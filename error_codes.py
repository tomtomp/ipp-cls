

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import sys

do_debug = False


class ErrorCodes:
    """
    Enumeration containing all the error codes.
    """

    ERR_PARAM = 1
    ERR_INPUT_FILE = 2
    ERR_OUTPUT_FILE = 3
    ERR_INPUT_FORMAT = 4
    ERR_INHERITANCE = 21


def print_err(*stuff):
    """
    Print given information to the stderr.
    :param stuff: Things to print out.
    :return: None
    """
    print(*stuff, file=sys.stderr)


def print_info(*stuff):
    """
    Print given information to the stdout.
    :param stuff: Things to print out.
    :return: None
    """
    if do_debug:
        print("INFO : ", end="")
        print(*stuff, file=sys.stdout)