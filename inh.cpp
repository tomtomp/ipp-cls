class A {};
class B : public A {int x;};
class C : public A {int x;};
class D : public B, C {};
class E : public B, C {};
class F : public D {};
class G : public E {};
class H : public F, G {};
