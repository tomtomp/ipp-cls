

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


from cpp_enums import AccessSpec


class CppUsing:
    """
    Represents a class using directive.
    """

    def __init__(self, class_from, name, access_spec):
        """
        Initialize using object.
        :param class_from: Class name from which we are using it, string.
        :param name: Name of member.
        :param access_spec: Access specifier of attribute, AccessSpec.
        :return:
        """

        self.__class_from = class_from
        self.__name = name

        if type(access_spec) is AccessSpec:
            self.__access_spec = access_spec
        else:
            raise Exception("Wrong parameter type")

    def get_class_from(self):
        """
        Get name of the class from which we are using something.
        :return: string
        """
        return self.__class_from

    def get_name(self):
        """
        Get the name of the member.
        :return: string
        """
        return self.__name

    def get_access_spec(self):
        """
        Get new access specifier of the member.
        :return: AccessSpec
        """
        return self.__access_spec

    def dump_info(self):
        """
        Dumps information about this attribute to the stdout.
        :return: None
        """

        print(self.__access_spec.value, " ", self.__class_from, " |-> ", self.__name)
