

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import sys
import xml.dom.minidom as MD
from xml.dom.minidom import Document
from lxml import etree as ET

from error_codes import ErrorCodes
from error_codes import print_err

from param_parser import ParamParser

from cpp_file import CppFile


class CppParser:
    """
    Main application class.
    """

    def __init__(self):
        """
        Initialize the main application attributes.
        :return: None
        """
        self.__param_parser = None

    def run(self):
        """
        Main application loop.
        :return:
        """

        self.__param_parser = ParamParser()

        if self.__param_parser.isset_help():
            print("TODO : help menu!")
        else:
            xml_out = self.process_file()
            self.write_file(xml_out)

    def process_file(self):
        """
        Process the input file
        :return: Returns string representation of xml file.
        """

        output_content = ""

        try:
            if self.__param_parser.get_input_filename():
                with open(self.__param_parser.get_input_filename(), "r") as file_in:
                    input_content = file_in.read()
            else:
                input_content = sys.stdin.read()
        except IOError:
            print_err("Input file is not readable!")
            sys.exit(ErrorCodes.ERR_INPUT_FILE)

        parsed_file = CppFile(input_content, self.__param_parser.is_do_conflicts())
        parsed_file.apply_inheritance()

        detail = self.__param_parser.get_details()

        if detail or detail == "":
            out_doc = parsed_file.get_class_xml(detail)
        else:
            out_doc = parsed_file.get_inheritance_xml()

        if self.__param_parser.get_search():
            out_doc = self.use_xpath(out_doc, self.__param_parser.get_search())
        output_content = self.make_xml_pretty(out_doc, self.__param_parser.get_pretty_indent())

        return output_content

    def use_xpath(self, xml_doc, xpath):
        """
        Use given XPath search string on xml document and return result.
        :param xml_doc: MiniDom XML document.
        :param xpath: String containing XPath search querry.
        :return: MiniDom XML document.
        """
        my_tree = ET.ElementTree(ET.fromstring(xml_doc.toxml('utf-8')))
        search = my_tree.xpath(xpath)
        result = Document()
        result_node = result.createElement("result")
        result.appendChild(result_node)
        for part in search:
            if type(part) is ET._Element:
                result_node.appendChild(MD.parseString(ET.tostring(part, encoding='utf-8')).childNodes[0])
            else:
                result_node.appendChild(result.createTextNode(part))
        return result

    def write_file(self, xml_string):
        """
        Write the given xml in string format to the param_parsers output file.
        :param xml_string: String containing xml.
        :return:
        """

        try:
            if self.__param_parser.get_output_filename():
                with open(self.__param_parser.get_output_filename(), "w") as file_out:
                    file_out.write(xml_string)
            else:
                sys.stdout.write(xml_string)
        except IOError:
            print_err("Output file is not writeable!")
            sys.exit(ErrorCodes.ERR_OUTPUT_FILE)

    @staticmethod
    def make_xml_pretty(document, indent_size=4):
        """
        Prettify the xml output.
        :param document: MiniDom document.
        :param indent_size: Size of the indent in spaces.
        :return:
        """

        return document.toprettyxml(indent=" " * indent_size, encoding="utf-8").decode("utf-8")
