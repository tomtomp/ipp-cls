

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import sys

from error_codes import print_err
from error_codes import ErrorCodes

from cpp_class import CppClass
from cpp_enums import AccessSpec


class InheritanceNode:
    """
    This class represents a single inheritance node in inheritance tree.
    """

    def __init__(self, class_obj):
        """
        Initialize the inheritance node.
        :param class_obj: Class object, which this node represents, CppClass.
        """

        self.__class_obj = class_obj
        self.__inh_from = set()
        self.__inheritors = set()

    def __eq__(self, other):
        """
        Overloading equality operator.
        :param other: The other one.
        :return: bool
        """

        return self.get_class_obj() == other.get_class_obj()

    def __ne__(self, other):
        """
        Overloading inequality operator.
        :param other: The other one.
        :return: bool
        """

        return not (self == other)

    def __hash__(self):
        """
        Overloading hash operator.
        """

        return hash(self.get_class_obj().get_name())

    def get_class_obj(self):
        """
        Get class object, this node represents.
        :return: CppClass
        """

        return self.__class_obj

    def add_inheritor(self, class_node):
        """
        Add inheritor to the list of inheritors.
        :param class_node: InheritanceNode that inherits from this node.
        """

        self.__inheritors.add(class_node)

    def add_inh_from(self, class_node, access_spec):
        """
        Add node this node inherits from.
        :param class_node: InheritanceNode this node inherits from.
        :param access_spec: The acces specifier of the inheritance, AccessSpec.
        """

        self.__inh_from.add((class_node, access_spec))
        class_node.add_inheritor(self)

    def get_inheritors(self):
        """
        Get the list of inheritance nodes who inherit from this one.
        :return: List of InheritanceNode.
        """

        return self.__inheritors

    def get_inh_from(self):
        """
        Get list of inheritance nodes this nodes inherits from.
        :return: List of (InheritanceNode, AccessSpec).
        """

        return self.__inh_from

    def dump_info_inh_from(self, indent=0):
        """
        Dumps information about this InheritanceNode and all of the nodes it inherits from to the stdout.
        :param indent: How many space should this info dump use. Default is 0.
        """

        print(" " * indent, self.__class_obj.get_name(), sep="")

        for node in self.__inh_from:
            # Get the InheritanceNode
            node[0].dump_info_inheritors(indent + 1)

    def dump_info_inheritors(self, indent=0):
        """
        Dumps information about this InheritanceNode and all of the inheritor nodes to the stdout.
        :param indent: How many space should this info dump use. Default is 0.
        """

        print(" " * indent, self.__class_obj.get_name(), sep="")

        for node in self.__inheritors:
            # Get the InheritanceNode
            node.dump_info_inheritors(indent + 1)


class InheritanceTree:
    """
    This class represents an inheritance tree of all classes in single Cpp file.
    Can contain multiple sub-trees. Each class name has to be unique!
    """

    def __init__(self, class_dict):
        """
        Initialize the inheritance tree.
        :param class_dict: Dictionary of class objects, where key is the name of the class (string) and
                             the value is CppClass object, representing the class.
        """

        # Will contain a single node for each class name.
        self.__class_nodes = {}

        # Will contain class nodes, that have not been inherited by anyone.
        self.__not_inherited = {}

        # Will contain class nodes, that have no inherits.
        self.__no_inherits = {}

        for class_name, class_obj in class_dict.items():
            self.__add_class_node(class_name, class_obj)

        # Now that all of the structures are initialized, we can run the inheritance tree construction.
        # TODO - Cycle testing

        for class_name, class_node in self.__class_nodes.items():
            inherits = class_node.get_class_obj().get_inheritance_dict()

            # Iterate through PUBLIC, PRIVATE, PROTECTED inheritance lists.
            for inh_type, inh_list in inherits.items():
                # Iterate through the inheritances.
                for inh in inh_list:
                    # Get the name of the inherited class.
                    inh_class_name = inh.get_name()
                    # Get the node of the inherited class.
                    inh_class_node = self.__get_inherit_node(inh_class_name)

                    self.__del_no_inherits(class_name)
                    self.__del_not_inherited(inh_class_name)

                    # Add inheritance to the inherited class node.
                    class_node.add_inh_from(inh_class_node, inh_type)

        if self.__class_nodes and not self.__not_inherited and not self.__no_inherits:
            print_err("Error : Found an inheritance cycle!")
            sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

    def get_not_inherited(self):
        """
        Get dictionary with classes, that were not inherited.
        :return: Dictionary of string : InheritanceNode.
        """

        return self.__not_inherited

    def get_no_inherits(self):
        """
        Get dictionary with classes, that did not inherit anything.
        :return: Dictionary of string : InheritanceNode.
        """

        return self.__no_inherits

    def __add_class_node(self, class_name, class_obj):
        """
        Add given class object to the internal dictionaries.
        :param class_name: Name of the class, string.
        :param class_obj: Class object, CppClass.
        """

        class_node = InheritanceNode(class_obj)
        self.__class_nodes[class_name] = class_node
        self.__not_inherited[class_name] = class_node
        self.__no_inherits[class_name] = class_node

    def __get_inherit_node(self, class_name):
        """
        Get the InheritanceNode for given class_name.
        :param class_name: Name of the class, string.
        :return: InheritanceNode of the requested class.
        """

        try:
            return self.__class_nodes[class_name]
        except KeyError:
            print_err("Error : Inheriting from unknown class!", class_name)
            sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

    def __del_not_inherited(self, class_name):
        """
        Delete class from not_inherited dictionary, using the class_name.
        :param class_name: Name of the class, string.
        """

        try:
            del(self.__not_inherited[class_name])
        except KeyError:
            pass

    def __del_no_inherits(self, class_name):
        """
        Delete class from no_inherits dictionary, using the class_name.
        :param class_name: Name of the class, string.
        """

        try:
            del(self.__no_inherits[class_name])
        except KeyError:
            pass

    def dump_info(self):
        """
        Dump information about this InheritanceTree to the stdout.
        """

        print("Dumping info about inheritance tree (Inner inherits from the outer) : ")
        for node in self.__no_inherits.values():
            node.dump_info_inheritors()
        print("End of inheritance tree.")



