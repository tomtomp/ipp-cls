

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import getopt
import sys
import argparse

from error_codes import ErrorCodes
from error_codes import print_err


class ParamParser:
    """
    Used for parsing input parameters.
    """

    PARAM_ARRAY = ["help", "input=", "output=", "pretty-xml=",
                   "details", "search="]

    def __init__(self):
        """
        Get parameters and correctly parse them.
        :return:
        """

        # Attribute initialization
        self.__help = False
        self.__input_filename = ""
        self.__output_filename = ""
        self.__pretty_indent = 4
        self.__details = None
        self.__search = ""
        self.__input_filename_set = False
        self.__output_filename_set = False
        self.__do_conflicts = False
        # End of attribute initialization

        arg_parser = argparse.ArgumentParser(prog="CLS", description="Parse C++ file.", add_help=False)
        arg_parser.add_argument("--help", help="Display help.", default=False, action="store_true")
        arg_parser.add_argument("--input", help="Input C++ source file.")
        arg_parser.add_argument("--output", help="Output XML file.")
        arg_parser.add_argument("--pretty-xml", help="Number of indent spaces for each indent.")
        arg_parser.add_argument("--details", help="Input C++ source file.", nargs="?", const="", default=None)
        arg_parser.add_argument("--search", help="XPath search.")
        arg_parser.add_argument("--conflicts", help="Print conflicts.", default=False, action="store_true")

        try:
            result = arg_parser.parse_args(sys.argv[1:])
        except SystemExit:
            sys.exit(ErrorCodes.ERR_PARAM)

        arg_num = 0

        if result.help:
            arg_num += 1
            self.__help = True

        if result.search == "":
            print_err("Error : Value for search is required.")
            sys.exit(ErrorCodes.ERR_PARAM)
        elif result.search:
            arg_num += 1
            if result.help:
                print_err("Error : Help can be the only argument.")
                sys.exit(ErrorCodes.ERR_PARAM)
            self.__search = result.search

        if result.input == "":
            print_err("Error : Value for input is required.")
            sys.exit(ErrorCodes.ERR_PARAM)
        elif result.input:
            arg_num += 1
            if result.help:
                print_err("Error : Help can be the only argument.")
                sys.exit(ErrorCodes.ERR_PARAM)
            self.__input_filename = result.input
            self.__input_filename_set = True

        if result.output == "":
            print_err("Error : Value for output is required.")
            sys.exit(ErrorCodes.ERR_PARAM)
        elif result.output:
            arg_num += 1
            if result.help:
                print_err("Error : Help can be the only argument.")
                sys.exit(ErrorCodes.ERR_PARAM)
            self.__output_filename = result.output
            self.__output_filename_set = True

        if result.pretty_xml:
            arg_num += 1
            if result.help:
                print_err("Error : Help can be the only argument.")
                sys.exit(ErrorCodes.ERR_PARAM)
            try:
                self.__pretty_indent = int(result.pretty_xml)
                if self.__pretty_indent < 0:
                    raise ValueError
            except ValueError:
                print_err("Pretty xml argument has to be <0, inf>!")
                sys.exit(ErrorCodes.ERR_PARAM)

        if result.details == "" or result.details:
            arg_num += 1
            if result.help:
                print_err("Error : Help can be the only argument.")
                sys.exit(ErrorCodes.ERR_PARAM)
            self.__details = result.details

        if result.conflicts:
            arg_num += 1
            if result.help:
                print_err("Error : Help can be the only argument.")
                sys.exit(ErrorCodes.ERR_PARAM)
            if self.__details is None:
                print_err("Error : Conflicts argument can only be passed when details argument is entered.")
                sys.exit(ErrorCodes.ERR_PARAM)
            self.__do_conflicts = True

        if arg_num != len(sys.argv) - 1:
            print_err("Error : Number of parsed arguments does not equal the total number of arguments.")
            sys.exit(ErrorCodes.ERR_PARAM)

        """

        # Try to get the parameters
        try:
            opts, args = getopt.getopt(sys.argv[1:], "", self.PARAM_ARRAY)
        except getopt.GetoptError as err:
            print_err(str(err))
            sys.exit(ErrorCodes.ERR_PARAM)

        if args:
            print_err("Unknown additional parameters!")
            sys.exit(ErrorCodes.ERR_PARAM)

        # Main parameter processing loop
        for option, argument in opts:
            if option == "--help":
                self.__help = True
            elif option == "--input":
                self.__input_filename = argument
                self.__input_filename_set = True
            elif option == "--output":
                self.__output_filename = argument
                self.__output_filename_set = True
            elif option == "--pretty-xml":
                try:
                    self.__pretty_indent = int(argument)
                    if self.__pretty_indent < 0:
                        raise ValueError
                except ValueError:
                    print_err("Pretty xml argument has to be <0, inf>!")
                    sys.exit(ErrorCodes.ERR_PARAM)
            elif option == "--details":
                print(argument)
                self.__details = argument
            elif option == "--search":
                self.__search = argument
            else:
                print_err("This should not happen!")
                sys.exit(ErrorCodes.ERR_PARAM)
        # End of main parameter processing loop

        if self.__help and (self.__input_filename_set or self.__output_filename_set):
            print_err("Help has to be the only argument!")
            sys.exit(ErrorCodes.ERR_PARAM)

        """

    def isset_help(self):
        """
        Is the help argument set?
        :return: bool
        """
        return self.__help

    def get_input_filename(self):
        """
        Get the input filename.
        :return: string
        """
        return self.__input_filename

    def get_search(self):
        """
        Get the XPath search string.
        :return: string
        """
        return self.__search

    def get_output_filename(self):
        """
        Get the output filename.
        :return: string
        """
        return self.__output_filename

    def get_pretty_indent(self):
        """
        Is the pretty indent argument set?
        :return: bool
        """
        return self.__pretty_indent

    def get_details(self):
        """
        Get the input filename.
        :return: string
        """
        return self.__details

    def is_do_conflicts(self):
        """
        Should we do conflicts resolution?
        :return: bool
        """
        return self.__do_conflicts

    def get_search(self):
        """
        Get the input filename.
        :return: string
        """
        return self.__search

