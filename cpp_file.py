

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import sys

from xml.dom.minidom import getDOMImplementation as MDI
from xml.dom.minidom import Document

from cpp_enums import AccessSpec
from cpp_enums import TokenType

from cpp_scanner import CppScanner

from error_codes import print_err
from error_codes import print_info
from error_codes import ErrorCodes
import error_codes

from cpp_class import CppClass

from cpp_token import CppToken

from cpp_method import CppMethod

from cpp_attribute import CppAttribute

from cpp_using import CppUsing

from inh_tree import InheritanceTree
from inh_tree import InheritanceNode


class CppFile:
    """
    This class represents a file written in C++ and is able to parse it.
    """

    def __init__(self, cpp_source, do_conflicts):
        """
        Initialize CppFile class
        :param cpp_source: C++ source in single string.
        :param do_conflicts: Should we do conflict resolution?
        :return:
        """

        self.__cpp_source = cpp_source
        # Associative array to contain all the classes.
        self.__classes = {}

        # Inheritance XML document.
        self.__inh_doc = None

        # There will be InheritanceTree in this attribute.
        self.__inheritance_tree = None

        self.__do_conflicts = do_conflicts

        self.__analyze()

    def get_class_dict(self):
        """
        Get the class object dictionary. Key is name of the class (string) and value is the CppClass object.
        :return: dictionary
        """

        return self.__classes

    def apply_inheritance(self):
        """
        After the file is parsed, this method will apply all of the inheritance rules.
        """

        # Construct the inheritance tree.
        self.__inheritance_tree = InheritanceTree(self.__classes)

        # We will propagate attributes from the classes with the least number of inherited classes to the top (bottom).
        active_nodes = set(self.__inheritance_tree.get_no_inherits().values())
        dead_nodes = set()

        # While new_nodes not empty.
        while active_nodes:
            print_info("## New cycle")

            # Will contain all the newly discovered nodes.
            new_nodes = set()

            for node in active_nodes:
                print_info("Node : ", node.get_class_obj().get_name())

                # Get new_nodes from the active_nodes.
                inheritors = node.get_inheritors()
                # Get the nodes from who current node inherits.
                inh_from = node.get_inh_from()
                # Get the class object.
                class_obj = node.get_class_obj()
                # Get the class name.
                class_name = class_obj.get_name()

                # Test, if all the inheritances were already done.
                all_available = True
                for inh_node, access_spec in inh_from:
                    if inh_node not in dead_nodes:
                        all_available = False
                        break

                if all_available:
                    # If all inheritances were done, then we can continue.
                    for inheritor in inheritors:
                        if inheritor not in dead_nodes:
                            print_info("Adding new node for : ", inheritor.get_class_obj().get_name())
                            new_nodes.add(inheritor)
                    # If the node is already in dead_nodes, something is wrong.
                    if node in dead_nodes:
                        print_err("Error : Detected a cycle in the inheritance!", class_name)
                        sys.exit(ErrorCodes.ERR_INPUT_FORMAT)
                    # Move the active node to the dead_nodes.
                    print_info("Adding dead node for : ", node.get_class_obj().get_name())
                    dead_nodes.add(node)
                    for inh_node, access_spec in inh_from:
                        # Apply the inheritance for the active_nodes.
                        print_info("Applying inheritance for class : ", node.get_class_obj().get_name(), " ",
                              access_spec.value, " ", inh_node.get_class_obj().get_name())
                        class_obj.process_usings(self.__classes)
                        class_obj.inherit_from(inh_node.get_class_obj(), access_spec, self.__do_conflicts)
                    try:
                        new_nodes.remove(node)
                    except KeyError:
                        pass
                    class_obj.remove_conflicted()
                else:
                    # Else wait for later.
                    print_info("Not all required inheritances were done!")
                    new_nodes.add(node)

            # Move new nodes to the active_nodes.
            active_nodes = new_nodes

    def get_class_xml(self, name=""):
        """
        Get the class XML document.
        :param name: Name of the requested class, will process all, if empty.
        :return: MiniDom document.
        """

        # Initialize the XML document.
        impl = MDI()

        if name:
            class_doc = Document()
            class_doc.appendChild(self.__classes[name].get_xml_root(class_doc))
        else:
            class_doc = impl.createDocument(None, "model", None)
            root = class_doc.documentElement

            for class_obj in self.__classes.values():
                root.appendChild(class_obj.get_xml_root(class_doc))

        return class_doc

    def get_inheritance_xml(self):
        """
        Get the inheritance XML document.
        :return: MiniDom document.
        """

        # We will be constructing the XML in a way, that the classes, which were never inherited will be on the outside.
        inh_nodes = self.__inheritance_tree.get_no_inherits().values()

        # Initialize the XML document.
        impl = MDI()
        self.__inh_doc = impl.createDocument(None, "model", None)
        root = self.__inh_doc.documentElement

        for node in inh_nodes:
            root.appendChild(self.__create_inheritance_tag(node))

        return self.__inh_doc

    def __create_inheritance_tag(self, inh_node):
        """
        Recursively create an inheritance tag and return it.
        :param inh_node: Node from which to start the creation.
        :return: MiniDom Element.
        """

        class_obj = inh_node.get_class_obj()
        class_tag = self.__inh_doc.createElement("class")
        class_tag.setAttribute("name", class_obj.get_name())
        class_tag.setAttribute("kind", class_obj.get_kind().value)

        for node in inh_node.get_inheritors():
            class_tag.appendChild(self.__create_inheritance_tag(node))

        return class_tag

    def __analyze(self):
        """
        Analyzes the input source file and fills the _classes list with data.
        :return:
        """

        # Setup the token system.
        self.__scanner = CppScanner(self.__cpp_source)
        self.__tokens = self.__scanner.token_gen()
        self.__prep_token()

        # And run the recursive descent analyzer.
        self.__rec_descent()

        if error_codes.do_debug:
            for class_obj in self.__classes.values():
                class_obj.dump_info()

    def __prep_token(self):
        """
        Prepare the next token. Sets the _curr_token attribute.
        :return: (type, content)
        """

        try:
            self.__curr_token = next(self.__tokens)
            return self.__curr_token.get_type(), self.__curr_token.get_content()
        except StopIteration:
            # If, by chance, someone called this method after the EOF was returned,
            #   just create new EOF token.
            self.__curr_token = CppToken(TokenType.EOF)

    def __get_token_type(self):
        """
        Get the token type of current token.
        :return:
        """

        return self.__curr_token.get_type()

    def __get_token_content(self):
        """
        Get the token content of current token.
        :return:
        """

        return self.__curr_token.get_content()

    def get_class_obj(self, class_name):
        """
        Get the class object, by its name.
        :param class_name: Name of class, string.
        :return: CppClass or None, if the class object was not found.
        """

        try:
            class_obj = self.__classes[class_name]
            return class_obj
        except KeyError:
            # If the class object was not found
            return None

    def __add_class_obj(self, class_name, class_obj):
        """
        Add the given CppClass object to the list, rewrites the original.
        :param class_name: Class name, string.
        :param class_obj: CppClass object.
        :return:
        """

        self.__classes[class_name] = class_obj

    def __check_colon(self):
        """
        Check if there is a colon after the current token and exit program,
          if it is missing.
        :return: Returns True, if the colon was found.
        """

        self.__prep_token()
        if self.__get_token_type() != TokenType.COLON:
            print_err("Error : Missing colon.")
            exit(ErrorCodes.ERR_INPUT_FORMAT)

        return True

    def __rec_descent(self):
        """
        This is the beginning of the recursive descent.
        :return:
        """

        print_info("Starting recursive descent!")
        while self.__get_token_type() != TokenType.EOF:

            # Search for the next class
            if self.__get_token_type() == TokenType.KW_CLASS:
                print_info("Found a class")
                self.__prep_token()
                self.__rec_class_kw()
                print_info("End of class", self.__get_token_type())
                # Skip the prep_token
                continue
            # else -> for now, lets ignore anything else

            self.__prep_token()
        print_info("End of recursive descent!")

    def __rec_class_kw(self):
        """
        Recursive descent, found class keyword.
            class-key attr identifier ;
        :return:
        """

        # After the class keyword, there should be class name.
        # TODO - maybe add the C++11 attributes.
        if self.__get_token_type() == TokenType.IDENT:
            class_name = self.__get_token_content()
            print_info("Found the class identifier : ", class_name)

            # Try to get the class object.
            class_obj = self.get_class_obj(class_name)
            # If this is the first occurrence, create new class object.
            if not class_obj:
                class_obj = CppClass(class_name)
            # Continue in recursive descent and add the result to the list.
            self.__prep_token()
            self.__add_class_obj(class_name, self.__rec_class(class_obj))
        else:
            print_err("There should be class identifier after class keyword")
            exit(ErrorCodes.ERR_INPUT_FORMAT)

    def __rec_class(self, class_obj):
        """
        Recursive descent, parsing class name + inheritance.
        :param class_obj: CppClass object representing current class.
        :return: CppClass object.
        """

        # Check for inheritance.
        if self.__get_token_type() == TokenType.COLON:
            # Inheritance found -> we are in class definition.
            print_info("Class definition.")
            print_info("Found inheritance list.")
            # Default access specifier is PRIVATE.
            access_specifier = AccessSpec.PRIVATE
            is_virtual = False

            self.__prep_token()
            # Parsing the list of inheritance.
            # The list ends with '{'
            while self.__get_token_type() != TokenType.CURL_L:
                token_type = self.__get_token_type()
                if token_type == TokenType.KW_PUBLIC:
                    print_info("Found access specifier : PUBLIC.")
                    access_specifier = AccessSpec.PUBLIC
                elif token_type == TokenType.KW_PRIVATE:
                    print_info("Found access specifier : PRIVATE.")
                    access_specifier = AccessSpec.PRIVATE
                elif token_type == TokenType.KW_PROTECTED:
                    print_info("Found access specifier : PROTECTED.")
                    access_specifier = AccessSpec.PROTECTED
                elif token_type == TokenType.KW_VIRTUAL:
                    print_info("Found virtual specifier : VIRTUAL.")
                    is_virtual = True
                elif token_type == TokenType.IDENT:
                    print_info("Found identifier, adding new inheritance : ",
                               "\n\tName : ", self.__get_token_content(),
                               "\n\tVirtual : ", is_virtual, "\n\tAccess specifier :",
                               access_specifier)
                    class_obj.add_inheritance(self.__get_token_content(),
                                              access_specifier, is_virtual)
                    # Reset the inheritance access specifier
                    access_specifier = AccessSpec.PRIVATE
                self.__prep_token()

            print_info("End of inheritance list.")
            # End of inheritance parsing.

        # There has to be a { here.
        if self.__get_token_type() == TokenType.CURL_L:

            self.__prep_token()

            self.__rec_class_inside(class_obj)

            # There has to be a } here.
            if self.__get_token_type() != TokenType.CURL_R:
                print_err("Error : No ending } in class definition!")
                sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

            self.__prep_token()

        if self.__get_token_type() == TokenType.SEMICOLON:
            self.__prep_token()
            return class_obj
        else:
            # Error, no other token can be here.
            print_err("Error : Wrong token after class identifier : ", self.__get_token_type())
            sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

    def __rec_class_inside(self, class_obj):
        """
        Recursive descent, inside of class definition.
        :param class_obj: CppClass object representing current class.
        :return: CppClass object.
        """

        # Default access specifier is PRIVATE
        access_specifier = AccessSpec.PRIVATE
        class_name = class_obj.get_name()

        token_type = self.__get_token_type()

        print_info("Inside class definition.")
        while token_type != TokenType.CURL_R:

            if token_type == TokenType.KW_PUBLIC:
                print_info("Public access specifier.")
                access_specifier = AccessSpec.PUBLIC
                self.__check_colon()
            elif token_type == TokenType.KW_PRIVATE:
                print_info("Private access specifier.")
                access_specifier = AccessSpec.PRIVATE
                self.__check_colon()
            elif token_type == TokenType.KW_PROTECTED:
                print_info("Protected access specifier.")
                access_specifier = AccessSpec.PROTECTED
                self.__check_colon()
            elif token_type == TokenType.IDENT:
                # We have an attribute, method or constructor.
                self.__rec_member(class_obj, access_specifier)
            elif token_type == TokenType.TILDE:
                # We have a destructor.
                self.__prep_token()
                destructor_method = self.__rec_destructor(class_name, access_specifier)
                class_obj.set_destructor(destructor_method, access_specifier)
            else:
                self.__rec_member(class_obj, access_specifier)
                """
                print_err("Error : Unknown character inside class definition! : ", token_type)
                sys.exit(ErrorCodes.ERR_INPUT_FORMAT)
                """

            self.__prep_token()
            token_type = self.__get_token_type()
        print_info("End of class definition.")

        return class_obj

    def __rec_destructor(self, class_name, access_specifier):
        """
        Destructor parsing

        ~ class_name ( ) { } ;

        :param class_name: Name of the class, string.
        :param access_specifier: Access specifier of this destructor, AccessSpec.
        :return: Returns the CppMethod of destructor.
        """

        print_info("Destructor for : ", class_name)
        print_info("Token : ", self.__get_token_type(), " ", self.__get_token_content())

        if self.__get_token_type() == TokenType.IDENT and self.__get_token_content() == class_name:

            destructor_method = CppMethod("void", class_name, access_specifier, class_name)

            self.__prep_token()

            # (
            if self.__get_token_type() != TokenType.ROUND_L:
                print_err("Error : After destructor name only empty pair of brackets can be present!",
                          self.__get_token_type())
                sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

            self.__prep_token()

            # )
            if self.__get_token_type() != TokenType.ROUND_R:
                print_err("Error : After destructor name only empty pair of brackets can be present!",
                          self.__get_token_type())
                sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

            self.__prep_token()

            # Is the destructor defined? { }
            if self.__get_token_type() == TokenType.CURL_L:
                print_info("It is defined")

                self.__prep_token()

                if self.__get_token_type() == TokenType.CURL_R:
                    # The destructor is defined.
                    destructor_method.set_defined()
                else:
                    print_err("Found a lone opening curly bracket!")
                    sys.exit(ErrorCodes.ERR_INPUT_FORMAT)
            # ;
            elif self.__get_token_type() != TokenType.SEMICOLON:
                print_err("Error : After destructor name only empty pair of brackets and a semicolon ",
                          "can be present!", self.__get_token_type())
                sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

            return destructor_method
        else:
            print_err("Error : Destructor identifier has to be the same as the class name!")
            sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

    def __rec_member(self, class_obj, access_specifier):
        """
        Parsing of a class member.
            [static] [const] type attr_name ;
            [static] [const] ret_type method_name ( param_list... ) [{ }] ;
            class_name ( param_list ) [{ }] ;
        :param class_obj: Current class object, CppClass.
        :param access_specifier: Access specifier of this member.
        :return:
        """

        print_info("Inside member parsing.")

        class_name = class_obj.get_name()
        static = False
        virtual = False
        found_primary_type = False
        found_name = False
        in_brack = False
        name = ""
        first_type = ""
        second_type = ""
        token_type = self.__get_token_type()
        token_content = self.__get_token_content()

        if token_type == TokenType.IDENT and token_content == class_name:
            first_type = token_content
            token_type, token_content = self.__prep_token()
            if token_type == TokenType.ROUND_L:
                # We found a constructor.
                print_info("Found a constructor.")
                class_constructor =  self.__rec_method(class_name, access_specifier, class_name, class_name)
                class_obj.add_constructor(class_constructor, access_specifier)
                return
            else:
                # It is just a part of the first type...
                pass

        print_info("Parsing member.")
        # Parsing the member, until either semicolon is found and no name was found, or right curly bracket.
        while not ((token_type == TokenType.SEMICOLON and not found_name) or token_type == TokenType.CURL_R):

            if token_type == TokenType.KW_STATIC:
                static = True

            elif token_type == TokenType.KW_CONST:
                first_type += " const "

            elif token_type == TokenType.ASTER:
                second_type += " * "

            elif token_type == TokenType.AMPER:
                second_type += " & "

            elif token_type == TokenType.BRACK_L:
                second_type += " [ "
                in_brack = True

            elif token_type == TokenType.BRACK_R:
                second_type += " ] "
                in_brack = False

            elif token_type == TokenType.NUM and in_brack:
                second_type += token_content

            elif token_type == TokenType.KW_VIRTUAL:
                virtual = True

            elif token_type == TokenType.TILDE:
                # We found a destructor.
                if found_primary_type:
                    print_err("Error : Destructor does not have a return type!")
                    sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

                token_type, token_content = self.__prep_token()

                destructor_method = self.__rec_destructor(class_name, access_specifier)
                if virtual:
                    destructor_method.set_virtual()
                class_obj.set_destructor(destructor_method)
                continue

            elif token_type == TokenType.IDENT and not found_primary_type:
                first_type += token_content
                found_primary_type = True

            elif token_type == TokenType.IDENT and found_primary_type:
                # Attribute identifier or method name.
                name = token_content
                found_name = True

            elif token_type == TokenType.ROUND_L and found_name:
                # We found a method.
                print_info("Found a method!", name)
                method = self.__rec_method(name, access_specifier, first_type + second_type, class_name)
                if static:
                    method.set_static()
                if virtual:
                    method.set_virtual()
                class_obj.add_method(method, access_specifier)
                static = False
                # Skip the prep_token
                found_name = False
                token_type = self.__get_token_type()
                token_content = self.__get_token_content()
                print_info("Skipping the prep_token - method", token_type)
                continue

            elif token_type == TokenType.COMMA or token_type == TokenType.SEMICOLON and found_name:
                # We found a list of attributes.
                print_info("Found a list of attributes!", name, " ", first_type + second_type)
                print_info("First :", first_type, "second :", second_type)
                attribute = CppAttribute(first_type + second_type, name, access_specifier, class_name)
                if static:
                    attribute.set_static()
                class_obj.add_attribute(attribute, access_specifier)
                second_type = ""
                static = False
                found_name = False
                # Don't skip the semicolon
                if token_type == TokenType.SEMICOLON:
                    continue

            elif token_type == TokenType.KW_USING:
                # We found a using directive!

                token_type, token_content = self.__prep_token()

                # Next is the name of the class.
                if token_type != TokenType.IDENT:
                    print_err("Error : After using keyword, there should be a class name!")
                    sys.exit(ErrorCodes.ERR_INPUT_FORMAT)
                using_class = token_content

                # Next, there should be 2 colons.
                token_type, token_content = self.__prep_token()
                if token_type != TokenType.COLON:
                    print_err("Error : After using and class name, there should be 2 colons!")
                    sys.exit(ErrorCodes.ERR_INPUT_FORMAT)
                token_type, token_content = self.__prep_token()
                if token_type != TokenType.COLON:

                    sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

                # Now there should be a identifier of the class member.
                token_type, token_content = self.__prep_token()
                if token_type != TokenType.IDENT:
                    print_err("Error : After using keyword, class name and 2 colons there should be a"
                              " member identifier!")
                    sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

                using_directive = CppUsing(using_class, token_content, access_specifier)

                class_obj.add_using(using_directive, access_specifier)

            token_type, token_content = self.__prep_token()
        # End of member parsing
        print_info("End of member parsing. :", first_type, "|", second_type, "|", name, "|")

    def __rec_method(self, method_name, access_specifier, ret_type, class_name):
        """
        Method arguments parsing.

        ( arguments... ) { }

        :param method_name: Name of the method, string.
        :param access_specifier: Access specifier of the method.
        :param ret_type: Method return type, string.
        :param class_name: Name of the class, string.
        :return: Returns the CppMethod of constructor.
        """

        method = CppMethod(ret_type, method_name, access_specifier, class_name)

        if self.__get_token_type() != TokenType.ROUND_L:
            print_err("Error : There should be a left bracket here!")
            sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

        token_type, token_content = self.__prep_token()

        in_brack = False
        arg_type = ""
        arg_name = ""
        found_primary_type = False

        # Process the list of arguments
        print_info("Starting argument parsing of : ", method_name, self.__get_token_type())
        while token_type != TokenType.ROUND_R:
            if token_type == TokenType.KW_CONST:
                arg_type += " const "

            elif token_type == TokenType.ASTER:
                arg_type += " * "

            elif token_type == TokenType.AMPER:
                arg_type += " & "

            elif token_type == TokenType.BRACK_L:
                arg_type += " [ "
                in_brack = True

            elif token_type == TokenType.BRACK_R:
                arg_type += " ] "
                in_brack = False

            elif token_type == TokenType.NUM and in_brack:
                arg_type += token_content

            elif token_type == TokenType.IDENT:
                if found_primary_type:
                    arg_name = token_content
                else:
                    arg_type += token_content
                    found_primary_type = True

            elif token_type == TokenType.COMMA:
                # End of current attribute
                print_info("Adding new attribute : ", arg_name)
                attribute = CppAttribute(arg_type, arg_name, AccessSpec.NONE, "")
                method.add_argument(attribute)
                arg_type = ""
                arg_name = ""
                found_primary_type = False

            token_type, token_content = self.__prep_token()

        # Add the last argument.
        if arg_type:
            print_info("Adding new attribute : ", arg_name)
            attribute = CppAttribute(arg_type, arg_name, AccessSpec.NONE, "")
            method.add_argument(attribute)

        print_info("End of argument parsing for : ", method_name)
        # End of argument list processing

        token_type, token_content = self.__prep_token()

        print_info("The next token is :", token_type)
        if token_type == TokenType.EQU:
            while token_type != TokenType.SEMICOLON and token_type != TokenType.CURL_R:
                token_type, token_content = self.__prep_token()

            return method
        elif token_type == TokenType.CURL_L:
            token_type, token_content = self.__prep_token()
            if token_type == TokenType.CURL_R:
                print_info("Method :", method_name, " defined!")
            else:
                print_err("Found a lone opening curly bracket!")
                sys.exit(ErrorCodes.ERR_INPUT_FORMAT)

        method.set_defined()

        return method



