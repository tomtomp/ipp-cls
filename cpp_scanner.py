

#CLS:xpolas34

"""
" Task CLS: C++ Classes in Python 3.4 for IPP 2015/2016
" Name and surname: Tomáš Polášek
" Login: xpolas34
"""


import sys
import string
from enum import Enum

from error_codes import ErrorCodes
from error_codes import print_err
from cpp_enums import TokenType
from cpp_token import CppToken


class ScannerState(Enum):
    """
    This class is enumeration of scanner finite state machine state.
    """

    START = 0
    IDENT = 1
    NUM = 2


class CppScanner:
    """
    This class function as a scanner - breaks up C++ source file into
      smaller pieces (tokens)
    """

    ## List containing all ASCII letters.
    ALPHA = list(string.ascii_letters)

    ## List containing all ASCII digits.
    NUM = list(string.digits)

    ## List containing whitespace characters.
    SPACE = ["\n", " ", "\t", "\r", "\f", "\v"]

    ## Dictionary containing keywords.
    KEYWORDS = {
        "class": TokenType.KW_CLASS,
        "virtual": TokenType.KW_VIRTUAL,
        "static": TokenType.KW_STATIC,
        "const": TokenType.KW_CONST,
        "public": TokenType.KW_PUBLIC,
        "private": TokenType.KW_PRIVATE,
        "protected": TokenType.KW_PROTECTED,
        "using": TokenType.KW_USING,
    }

    ## Dictionary containing translation from character to token name.
    TOKEN_NAMES = {
        "{": TokenType.CURL_L,
        "}": TokenType.CURL_R,
        "(": TokenType.ROUND_L,
        ")": TokenType.ROUND_R,
        "]": TokenType.BRACK_R,
        "[": TokenType.BRACK_L,
        ":": TokenType.COLON,
        ";": TokenType.SEMICOLON,
        ",": TokenType.COMMA,
        "~": TokenType.TILDE,
        "*": TokenType.ASTER,
        "&": TokenType.AMPER,
        "=": TokenType.EQU
    }

    def __init__(self, source):
        """
        Save the given source file.
        :param source: String containing C++ source code.
        :return:
        """
        self._source = source

    def token_gen(self):
        """
        Generator of tokens.
        :return: Generator object, which iterates through all the tokens in input
          source code.
        """

        cur_state = ScannerState.START
        next_state = ScannerState.START
        str_holder = ""
        index = 0

        # Main scanner loop
        while index < len(self._source):
            char = self._source[index]
            if cur_state == ScannerState.START:
                if char in CppScanner.ALPHA:
                    str_holder = char
                    next_state = ScannerState.IDENT
                elif char in CppScanner.NUM or char == "+" or char == "-":
                    str_holder = char
                    next_state = ScannerState.NUM
                elif char in CppScanner.TOKEN_NAMES.keys():
                    yield CppToken(CppScanner.TOKEN_NAMES[char], char)
                    next_state = ScannerState.START
                else:
                    next_state = ScannerState.START
                index += 1
            elif cur_state == ScannerState.IDENT:
                if char in CppScanner.ALPHA or char in CppScanner.NUM:
                    str_holder += char
                    next_state = ScannerState.IDENT
                    index += 1
                else:
                    if str_holder in CppScanner.KEYWORDS.keys():
                        yield CppToken(CppScanner.KEYWORDS[str_holder], str_holder)
                    else:
                        yield CppToken(TokenType.IDENT, str_holder)
                    next_state = ScannerState.START
                    # No increment here, because we need to process current char
            elif cur_state == ScannerState.NUM:
                if char in CppScanner.NUM:
                    str_holder += char
                    next_state = ScannerState.NUM
                    index += 1
                else:
                    yield CppToken(TokenType.NUM, str_holder)
                    next_state = ScannerState.START

            cur_state = next_state
        # End of main scanner loop

        yield CppToken(TokenType.EOF)
